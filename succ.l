
# Glossary:
(be functor arguments body)
you feed to ? the (cons functor arguments) and pilog unifies with the head in 
the database and substitutes with the body.

(be n ((0)))
(be n ((succ @A)) (n @A))


# (? (n (0)))
# (? (n (succ (succ (succ (0))))))
# (? (n (succ (0))))

Try to spot why they fail!
Does it match one element, or mistakenly several?

 Some mistaken ways to write the Succesor Function.
1
(be n ((0)))
(be n ((succ (@A))) (n (@A)))

Glossary
in (succ (@A)), the argument of succ got peeled
in (n (@A)), the variable @A got wrapped.

Lets explain this one:
from what I gather, the query that is built by (be sym args) and that can
be matched is of the form (cons sym args). So any query that you give to '?
must be of the form (sym ~args)

So the query must match:
(n (succ (@A)) (n (@A)))

when you run:
(? (n (succ (0))))
it works, but if you run
(? (n (succ (succ (0)))))
it fails.

Why is that?
well, we must check the arguments
some thing some thing arity. but the names of the arguments link! but they
are not unified by the arity, the number of arguments that you try to assign
to @A is unequal. how does it try to match the rule?

the query (n (succ (succ (0)))) is sent against:
          (n (succ (@A      )))
When you line them up, its obvious that you are trying to unify @A with two
arguments, succ and (0), but that cannot happen. Any variable can only match
1 other thing. So it fails.

This is trick because it seems to work for two other important cases:
(? (n (0)))
(? (n (succ (0))))
both of which return true.
But this is mostly coincidence.
1) the first one agrees with the rule (n (0)), so that doesn't even touch the 
recursive part.
2) The second one looks like this on unification:
    (n (succ (0 )))
    (n (succ (@A)))
the @A happens to match the 0, which gets passed to the body (n (@A)), which
evaluates to (n (0)), which is a fact and is proven right. But it only worked
because succ happened to have 1 argument, that @A was tried to unified with one
thing only, 0, instead of 2, succ and (0). For any other query:
(n (succ (succ (succ (0)))))
(n (succ (succ (0))))
@A is attempted to unify with two arguments, and it fail.


2
(be n ((0))
(be n ((succ (@A))) (n @A))

the same problem as 1, but this time the extra parentheses are not added in 
(n @A), so the query looks like (n 0) if it even works, so it fails at the 
fact level too.

these two fail in the same way: miscounted parenthesis.


